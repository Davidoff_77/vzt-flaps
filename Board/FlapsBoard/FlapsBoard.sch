EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Air Flaps Schema"
Date "2021-08-22"
Rev "2"
Comp "David Hruby"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text GLabel 3925 1175 2    50   Input ~ 0
+6V
Text GLabel 3925 1475 2    50   Input ~ 0
GND
$Comp
L Connector_Generic:Conn_01x03 J1
U 1 1 600E2FDF
P 1275 2725
F 0 "J1" H 1193 2400 50  0000 C CNN
F 1 "Conn_01x03" H 1193 2491 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Horizontal" H 1275 2725 50  0001 C CNN
F 3 "~" H 1275 2725 50  0001 C CNN
	1    1275 2725
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J2
U 1 1 600E5A04
P 1275 3250
F 0 "J2" H 1193 2925 50  0000 C CNN
F 1 "Conn_01x03" H 1193 3016 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Horizontal" H 1275 3250 50  0001 C CNN
F 3 "~" H 1275 3250 50  0001 C CNN
	1    1275 3250
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J3
U 1 1 600E5E6C
P 1275 3775
F 0 "J3" H 1193 3450 50  0000 C CNN
F 1 "Conn_01x03" H 1193 3541 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Horizontal" H 1275 3775 50  0001 C CNN
F 3 "~" H 1275 3775 50  0001 C CNN
	1    1275 3775
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J4
U 1 1 600E60D7
P 1275 4300
F 0 "J4" H 1193 3975 50  0000 C CNN
F 1 "Conn_01x03" H 1193 4066 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Horizontal" H 1275 4300 50  0001 C CNN
F 3 "~" H 1275 4300 50  0001 C CNN
	1    1275 4300
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J5
U 1 1 600E62B4
P 1275 4825
F 0 "J5" H 1193 4500 50  0000 C CNN
F 1 "Conn_01x03" H 1193 4591 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Horizontal" H 1275 4825 50  0001 C CNN
F 3 "~" H 1275 4825 50  0001 C CNN
	1    1275 4825
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J6
U 1 1 600E64D0
P 1275 5350
F 0 "J6" H 1193 5025 50  0000 C CNN
F 1 "Conn_01x03" H 1193 5116 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Horizontal" H 1275 5350 50  0001 C CNN
F 3 "~" H 1275 5350 50  0001 C CNN
	1    1275 5350
	-1   0    0    1   
$EndComp
Wire Wire Line
	1475 2825 1800 2825
Wire Wire Line
	1800 2825 1800 2650
Wire Wire Line
	1475 3350 1875 3350
Wire Wire Line
	1875 3350 1875 2750
Wire Wire Line
	1475 3875 1975 3875
Wire Wire Line
	1975 3875 1975 2850
Wire Wire Line
	1475 4400 2075 4400
Wire Wire Line
	2075 4400 2075 2950
Wire Wire Line
	1475 4925 2175 4925
Wire Wire Line
	2175 4925 2175 3050
Wire Wire Line
	1475 5450 2250 5450
Wire Wire Line
	2250 5450 2250 3150
Text GLabel 1550 2625 2    50   Input ~ 0
GND
Text GLabel 1550 3150 2    50   Input ~ 0
GND
Text GLabel 1550 3675 2    50   Input ~ 0
GND
Text GLabel 1550 4200 2    50   Input ~ 0
GND
Text GLabel 1550 4725 2    50   Input ~ 0
GND
Text GLabel 1550 5250 2    50   Input ~ 0
GND
Wire Wire Line
	1475 2625 1550 2625
Wire Wire Line
	1475 3150 1550 3150
Wire Wire Line
	1475 3250 1550 3250
Wire Wire Line
	1475 3675 1550 3675
Wire Wire Line
	1475 3775 1550 3775
Wire Wire Line
	1475 4200 1550 4200
Wire Wire Line
	1475 4300 1550 4300
Wire Wire Line
	1475 4725 1550 4725
Wire Wire Line
	1475 4825 1550 4825
Wire Wire Line
	1475 5250 1550 5250
Wire Wire Line
	1475 5350 1550 5350
$Comp
L Connector:Barrel_Jack_Switch J0
U 1 1 601C3EC2
P 1275 1275
F 0 "J0" H 1332 1592 50  0000 C CNN
F 1 "Barrel_Jack_Switch" H 1332 1501 50  0000 C CNN
F 2 "Connector_BarrelJack:BarrelJack_Horizontal" H 1325 1235 50  0001 C CNN
F 3 "~" H 1325 1235 50  0001 C CNN
	1    1275 1275
	1    0    0    -1  
$EndComp
Wire Wire Line
	1575 1175 1775 1175
$Comp
L Device:CP C1
U 1 1 60228F6B
P 3275 1325
F 0 "C1" H 3393 1371 50  0000 L CNN
F 1 "CP" H 3393 1280 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 3313 1175 50  0001 C CNN
F 3 "~" H 3275 1325 50  0001 C CNN
	1    3275 1325
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0103
U 1 1 60282F3C
P 1775 1175
F 0 "#FLG0103" H 1775 1250 50  0001 C CNN
F 1 "PWR_FLAG" H 1775 1348 50  0000 C CNN
F 2 "" H 1775 1175 50  0001 C CNN
F 3 "~" H 1775 1175 50  0001 C CNN
	1    1775 1175
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG0105
U 1 1 602EC6A9
P 3800 1475
F 0 "#FLG0105" H 3800 1550 50  0001 C CNN
F 1 "PWR_FLAG" H 3800 1648 50  0000 C CNN
F 2 "" H 3800 1475 50  0001 C CNN
F 3 "~" H 3800 1475 50  0001 C CNN
	1    3800 1475
	-1   0    0    1   
$EndComp
Wire Wire Line
	3800 1475 3925 1475
Text GLabel 4875 700  0    50   Input ~ 0
GND
$Comp
L CustomLib:SCD30 U5
U 1 1 603515C3
P 5650 1000
F 0 "U5" H 5650 1665 50  0000 C CNN
F 1 "SCD30" H 5650 1574 50  0000 C CNN
F 2 "CustomLib:MODULE_SCD30" H 5650 1000 50  0001 L BNN
F 3 "" H 5650 1000 50  0001 L BNN
F 4 "D1" H 5650 1000 50  0001 L BNN "PARTREV"
F 5 "Sensirion" H 5650 1000 50  0001 L BNN "MANUFACTURER"
F 6 "7.0mm" H 5650 1000 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 7 "Manufacturer Recommendations" H 5650 1000 50  0001 L BNN "STANDARD"
	1    5650 1000
	-1   0    0    1   
$EndComp
Wire Wire Line
	6250 900  6725 900 
Wire Wire Line
	6250 1000 6500 1000
Wire Wire Line
	4875 700  5050 700 
NoConn ~ 6250 1200
NoConn ~ 5050 1000
NoConn ~ 5050 1200
Wire Wire Line
	6725 900  6725 2050
NoConn ~ 1575 1275
$Comp
L CustomLib:YAAJ_DCDC_StepDown_LM2596 U2
U 1 1 603EE253
P 2575 1275
F 0 "U2" H 2575 1730 50  0000 C CNN
F 1 "YAAJ_DCDC_StepDown_LM2596" H 2575 1639 50  0000 C CNN
F 2 "CustomLib:YAAJ_DCDC_StepDown_LM2596" H 2575 1548 50  0000 C CNN
F 3 "" H 2525 1275 50  0001 C CNN
	1    2575 1275
	1    0    0    -1  
$EndComp
Wire Wire Line
	1775 1175 1925 1175
Connection ~ 1775 1175
Wire Wire Line
	2175 1375 2175 1575
Wire Wire Line
	2175 1575 2475 1575
Wire Wire Line
	2675 1575 2975 1575
Wire Wire Line
	2975 1175 3275 1175
Connection ~ 3800 1475
Wire Wire Line
	2975 1475 2975 1575
$Comp
L Regulator_Switching:TSR_1-2433 U1
U 1 1 61237A8C
P 2550 2050
F 0 "U1" H 2550 2417 50  0000 C CNN
F 1 "TSR_1-2433" H 2550 2326 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_TRACO_TSR-1_THT" H 2550 1900 50  0001 L CIN
F 3 "http://www.tracopower.com/products/tsr1.pdf" H 2550 2050 50  0001 C CNN
	1    2550 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	1925 1175 1925 1950
Wire Wire Line
	1925 1950 2150 1950
Wire Wire Line
	2175 1575 2050 1575
Wire Wire Line
	2050 1575 2050 2250
Connection ~ 2175 1575
Text GLabel 3925 1950 2    50   Input ~ 0
+3.3V
$Comp
L Device:CP C2
U 1 1 61249DE8
P 3275 1725
F 0 "C2" H 3393 1771 50  0000 L CNN
F 1 "CP" H 3393 1680 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D10.0mm_P5.00mm" H 3313 1575 50  0001 C CNN
F 3 "~" H 3275 1725 50  0001 C CNN
	1    3275 1725
	-1   0    0    1   
$EndComp
Connection ~ 3275 1175
Connection ~ 3275 1475
Wire Wire Line
	3275 1475 2975 1475
Wire Wire Line
	3275 1475 3800 1475
Wire Wire Line
	2950 1950 3275 1950
Wire Wire Line
	3275 1475 3275 1575
Wire Wire Line
	3275 1175 3925 1175
Wire Wire Line
	3275 1875 3275 1950
Connection ~ 3275 1950
Wire Wire Line
	1475 2725 1550 2725
Text GLabel 1550 2725 2    50   Input ~ 0
+6V
Text GLabel 1550 3250 2    50   Input ~ 0
+6V
Text GLabel 1550 3775 2    50   Input ~ 0
+6V
Text GLabel 1550 4300 2    50   Input ~ 0
+6V
Text GLabel 1550 4825 2    50   Input ~ 0
+6V
Text GLabel 1550 5350 2    50   Input ~ 0
+6V
$Comp
L CustomLib:BME280-3.3V U4
U 1 1 6125E54D
P 5225 2400
F 0 "U4" H 5142 1635 50  0000 C CNN
F 1 "BME280-3.3V" H 5142 1726 50  0000 C CNN
F 2 "CustomLib:BME280-3.3V" H 5225 2450 50  0001 C CNN
F 3 "" H 5225 2450 50  0001 C CNN
	1    5225 2400
	-1   0    0    1   
$EndComp
Text GLabel 5575 2350 2    50   Input ~ 0
+3.3V
Text GLabel 5575 2250 2    50   Input ~ 0
GND
Wire Wire Line
	5475 2250 5575 2250
Wire Wire Line
	5475 2350 5575 2350
Wire Wire Line
	6500 1000 6500 2150
Wire Wire Line
	6500 2150 5475 2150
Wire Wire Line
	5475 2050 6725 2050
NoConn ~ 5475 1850
NoConn ~ 5475 1950
Wire Wire Line
	5550 4775 5625 4775
Wire Wire Line
	5550 3475 5625 3475
Text GLabel 5625 3475 2    50   Input ~ 0
GND
Text GLabel 5625 4775 2    50   Input ~ 0
+3.3V
Wire Wire Line
	3675 4575 3675 5150
Wire Wire Line
	3675 5150 6500 5150
Wire Wire Line
	6500 2150 6500 5150
Connection ~ 6500 2150
Wire Wire Line
	3500 4275 3500 5300
Wire Wire Line
	3500 5300 6725 5300
Wire Wire Line
	6725 5300 6725 2050
Connection ~ 6725 2050
Wire Wire Line
	1800 2650 3850 2650
Wire Wire Line
	3850 2650 3850 3575
Wire Wire Line
	1875 2750 3775 2750
Wire Wire Line
	3775 2750 3775 3675
Wire Wire Line
	1975 2850 3675 2850
Wire Wire Line
	3675 2850 3675 3775
Wire Wire Line
	2075 2950 3600 2950
Wire Wire Line
	3600 2950 3600 3875
Wire Wire Line
	2175 3050 3525 3050
Wire Wire Line
	3525 3050 3525 3975
Wire Wire Line
	2250 3150 3375 3150
Wire Wire Line
	3375 3150 3375 4075
NoConn ~ 5550 2975
NoConn ~ 5550 3075
NoConn ~ 5550 3175
NoConn ~ 5550 3275
NoConn ~ 5550 3375
NoConn ~ 5550 3575
NoConn ~ 5550 3675
NoConn ~ 5550 3775
NoConn ~ 5550 3875
NoConn ~ 5550 3975
NoConn ~ 5550 4075
NoConn ~ 5550 4175
NoConn ~ 5550 4275
NoConn ~ 5550 4375
NoConn ~ 5550 4475
NoConn ~ 5550 4575
NoConn ~ 5550 4675
NoConn ~ 3950 4675
NoConn ~ 3950 4475
NoConn ~ 3950 4375
NoConn ~ 3950 3475
NoConn ~ 3950 3375
NoConn ~ 3950 3275
NoConn ~ 3950 3175
NoConn ~ 3950 3075
NoConn ~ 3950 2975
Wire Wire Line
	3950 4575 3675 4575
Wire Wire Line
	3375 4075 3950 4075
Wire Wire Line
	3525 3975 3950 3975
Wire Wire Line
	3600 3875 3950 3875
Wire Wire Line
	3675 3775 3950 3775
Wire Wire Line
	3775 3675 3950 3675
Wire Wire Line
	3850 3575 3950 3575
$Comp
L CustomLib:ESP32-DEVKITC-32U U3
U 1 1 6126BB6E
P 4750 3875
F 0 "U3" H 4750 5042 50  0000 C CNN
F 1 "ESP32-DEVKITC-32U" H 4750 4951 50  0000 C CNN
F 2 "CustomLib:MODULE_ESP32-DEVKITC-32U" H 4750 3875 50  0001 L BNN
F 3 "" H 4750 3875 50  0001 L BNN
F 4 "N/A" H 4750 3875 50  0001 L BNN "PARTREV"
F 5 "ESPRESSIF" H 4750 3875 50  0001 L BNN "MANUFACTURER"
F 6 "Manufacturer Recommendations" H 4750 3875 50  0001 L BNN "STANDARD"
	1    4750 3875
	-1   0    0    1   
$EndComp
Wire Wire Line
	3500 4275 3950 4275
Text GLabel 4875 1400 0    50   Input ~ 0
+3.3V
Wire Wire Line
	4875 1400 5050 1400
Wire Wire Line
	2050 2250 2550 2250
Wire Wire Line
	3275 1950 3925 1950
Text GLabel 2700 2250 2    50   Input ~ 0
GND
Wire Wire Line
	2550 2250 2700 2250
Connection ~ 2550 2250
Wire Wire Line
	1575 1375 2175 1375
Wire Wire Line
	2175 1175 1925 1175
Connection ~ 1925 1175
Text GLabel 3900 4775 0    50   Input ~ 0
GND
Text GLabel 3900 4175 0    50   Input ~ 0
GND
Wire Wire Line
	3900 4175 3950 4175
Wire Wire Line
	3900 4775 3950 4775
$EndSCHEMATC
