$fn=1000;

//------------------Variables-----------------
//Typ objektu
typObjektu = 2; //1 - horni cast klapky, 2 - spodni cast klapky, 3 - vnitrni cast, 4 - testovaci trubka, 5 - slepovaci trn

//Limec
vnejsiPrumer = 115;
svetlostTrubky = 93;
vnitrniPrumer = 97;
odskokHrany = 0;    
druhHrany = 0; //0 - zadna, 1 - horni roh, 2 - horni hrana, 3 -levy spodek, 4 -pravy spodek
tloustkaLimce = 1;

//Trubka
vyskaTrubky = 15;
vyskaVnitrniTrubky = 20;
tloustkaVnitrniTrubky = 1;
prumerOsy = 3;
vyskaOsy = 3;

//Servo Blok
vyskaServa = 23;
sirkaServa = 13;
vzdalenostSroubu = 27.5;
sirkaSroubu = 1.5;
hloubkaKanalku = 1;
sirkaKanalku = 5;
stredOsy = 5; //vzdalenost osy otaceni od hrany serva
vzdalenostOsy = 14.5; //vzdalenost spojky osy otaceni od hrany serva
odskokOsy = 16; //vzdalenost osy otaceni serva od hrany trubky
tloustkaBocniSteny = 2;
tloustkaBloku = 25;
tloustkaDrzaku = 10;
horniHranaBloku = 3;
umisteniVlevo = false;

//Vnitrni cast
tloustkaVnitrku = 3;
odskokOdTrubky = .5;
delkaOsy = 2.5;
vuleOsy = 0.5;
tloustkaTahla = 3;
vyskaOtvoru = 15;
prumerOtvoru = 2;
materialNadOtvorem = 3;
vzdalenostOdStredu = 0;

//Testovaci stojan
rezervaOdTrubky = 5;
vyskaStojan = 40;
tloustkaStojanu = 1;

//Slepovaci Trn
vyskaTrnu = 30;
tloustkaTrnu = 1;
vuleTrnu = 0.5;

//------------------Main-----------------
//vrchni cast
if(typObjektu == 1)
{
    union()
    {
        Limec(tloustkaLimce);
        VnitrniTrubka();
        mirror([umisteniVlevo?0:1,0,0])
        {
            ServoBlok();
        }
    }
}
else if(typObjektu == 2)
{
    union()
    {
        Limec(tloustkaLimce);
        SpodniTrubka();
    }
}
else if(typObjektu == 3)
{
    mirror([0,umisteniVlevo?0:1,0,])
    {
        VnitrniCast();
    }    
}
else if(typObjektu == 4)
{
    PomocnaTrubka(vyskaStojan, vnitrniPrumer + rezervaOdTrubky + tloustkaVnitrniTrubky, tloustkaVnitrniTrubky); 
}
else if(typObjektu == 5)
{
    PomocnaTrubka(vyskaTrnu, svetlostTrubky - vuleTrnu, tloustkaTrnu); 
}

//------------------Modules-----------------
module Limec(tloustka)
{
    difference()
    {  
        cylinder(h = tloustka, d = vnejsiPrumer, center = false);
        cylinder(h = tloustka, d = svetlostTrubky, center = false);
        
        if(druhHrany == 1)
        {
            Hrana(135, tloustka);
            Hrana(45, tloustka);
        }
        else if(druhHrany == 2)
        {
            Hrana(90, tloustka);            
        }
        else if(druhHrany == 3)
        {
            Hrana(135, tloustka);            
        }
        else if(druhHrany == 4)
        {
            Hrana(45, tloustka);            
        }
    }
}

module Hrana(uhel, tloustka)
{
    rotate([0, 0, uhel])
    {
        translate([vnitrniPrumer/2 + odskokHrany, -(vnitrniPrumer/2 + odskokHrany), 0])
        {
            cube([vnejsiPrumer, vnejsiPrumer, tloustka]);
        }
    }    
}

module VnitrniTrubka()
{
    difference()
    {  
        cylinder(h = vyskaTrubky, d = vnitrniPrumer);
        cylinder(h = vyskaTrubky, d = svetlostTrubky); 
        translate([-vnitrniPrumer / 2, 0, vyskaTrubky - vyskaOsy])
        {
            rotate([90,0,90])
            {
                cylinder(h = vnitrniPrumer, d = prumerOsy);
            };
        };
    }
}

module SpodniTrubka()
{
    difference()
    {  
        cylinder(h = vyskaVnitrniTrubky, d = svetlostTrubky + 2 * tloustkaVnitrniTrubky);
        cylinder(h = vyskaVnitrniTrubky, d = svetlostTrubky); 
    }
}

module PomocnaTrubka(vyskaPomocneTrubky, prumerPomocneTrubky, tloustkaPomocneTrubky)
{
    difference()
    {  
        cylinder(h = vyskaPomocneTrubky, d = prumerPomocneTrubky);
        cylinder(h = vyskaPomocneTrubky, d = prumerPomocneTrubky - (2 * tloustkaPomocneTrubky)); 
    }
}

module ServoBlok()
{    
    sroubOdHrany = (vzdalenostSroubu - vyskaServa)/2;
    osaVeVysce = vyskaTrubky - vyskaOsy;
    spodniHranaServa = osaVeVysce - stredOsy;
    vyskaBloku = spodniHranaServa + vyskaServa + sroubOdHrany + horniHranaBloku;
    
    hloubkaBloku = vnitrniPrumer/2 + odskokOsy + sirkaServa/2 + tloustkaBocniSteny;
    difference()
    {
        translate([-(tloustkaBloku + vzdalenostOsy), -hloubkaBloku, 0])
        {
            difference()
            {
    
                cube([tloustkaBloku, hloubkaBloku, vyskaBloku]);
                cube([tloustkaBloku - tloustkaDrzaku, sirkaServa + tloustkaBocniSteny, vyskaBloku]);
    
                translate([tloustkaBloku - tloustkaDrzaku, tloustkaBocniSteny + sirkaServa/2, spodniHranaServa - sroubOdHrany])
                {                    
                    rotate([90,0,90])
                    {
                        cylinder(h = tloustkaDrzaku, d = sirkaSroubu);
                    }
                }
                
                translate([tloustkaBloku - tloustkaDrzaku, tloustkaBocniSteny + sirkaServa/2, spodniHranaServa - sroubOdHrany + vzdalenostSroubu])
                {                    
                    rotate([90,0,90])
                    {
                        cylinder(h = tloustkaDrzaku, d = sirkaSroubu);
                    }
                }
                
                translate([tloustkaBloku - tloustkaDrzaku, tloustkaBocniSteny, spodniHranaServa])
                {
                    cube([tloustkaDrzaku, sirkaServa, vyskaServa]);
                }  

                translate([tloustkaBloku - tloustkaDrzaku, tloustkaBocniSteny + (sirkaServa -sirkaKanalku)/ 2, spodniHranaServa-hloubkaKanalku])
                {
                    cube([tloustkaDrzaku, sirkaKanalku, hloubkaKanalku]);
                }
            }
        }        
        
        cylinder(h = vyskaBloku, d = vnitrniPrumer);
    }
}
module VnitrniCast()
{
    prumerPlochy = svetlostTrubky - 2 * odskokOdTrubky;
    
    cylinder(h = tloustkaVnitrku, d = prumerPlochy);

    translate([0, (prumerPlochy + 2 * delkaOsy)/2, tloustkaVnitrku/2])
    {    
        rotate([90,0,0])
        {
            cylinder(h = prumerPlochy + 2 * delkaOsy, d = prumerOsy - vuleOsy);
        };
    } 
    
    vzdalenostOtvoru = vyskaOtvoru + tloustkaVnitrku;
    delkaHrany = vzdalenostOtvoru + prumerOtvoru/2 + materialNadOtvorem;
    translate([-(1/sin(45))*materialNadOtvorem,vzdalenostOdStredu,tloustkaVnitrku/2])
    {
        rotate([0, 45, 0])
        {
            difference()
            {
                cube([delkaHrany, tloustkaTahla, delkaHrany]);
                translate([materialNadOtvorem, tloustkaTahla, delkaHrany - materialNadOtvorem])
                {
                    rotate([90,0,0])
                    {
                        cylinder(h = tloustkaTahla, d = prumerOtvoru);
                    }
                }
                rotate([0, 45, 0])
                {
                    cube([delkaHrany, tloustkaTahla, delkaHrany*(1/sin(45))]);
                }
            }
        }         
    }
}