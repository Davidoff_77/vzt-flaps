# GCODE Files

- Nozzle diameter: 0.4mm
- Layer Height: 0.2mm
- Filament: PrintMe PLA from EazyFilament
- Nozzle temperature: 210°C
- Bed temperature: 40°C

## Bottom parts
- **Corner**
    - **1x** FlapBottomCorner_PLA_PLA0.2mm_19.7246g_0.4n_Ender3_2h6m.gcode 
    or 
    - **2x** FlapBottomCorner_Single_PLA_PLA0.2mm_9.88569g_0.4n_Ender3_1h3m.gcode
- **Down**
    - **1x** FlapBottomDown_PLA_PLA0.2mm_10.7965g_0.4n_Ender3_1h7m.gcode 
- **Right, Left and Up**
    - **3x** FlapBottomRightLeftUp_PLA_PLA0.2mm_10.3418g_0.4n_Ender3_1h5m.gcode

## Top parts
- **Corner**
    - **1x** FlapCorner_PLA_PLA0.2mm_43.8666g_0.4n_Ender3_4h10m.gcode
    or
    - **2x** FlapCorner_Single_PLA_PLA0.2mm_21.9697g_0.4n_Ender3_2h11m.gcode
- **Down**
    - **1x** FlapDown_PLA_PLA0.2mm_22.8809g_0.4n_Ender3_2h15m.gcode
- **Left**
    - **1x** FlapLeft_PLA_PLA0.2mm_22.4248g_0.4n_Ender3_2h13m.gcode
- **Right**
    - **1x** FlapRight_PLA_PLA0.2mm_22.4262g_0.4n_Ender3_2h13m.gcode
- **Up**
    - **1x** FlapUp_PLA_PLA0.2mm_22.4248g_0.4n_Ender3_2h13m.gcode

## Other parts
- **2x** FlapIn_PLA_PLA0.2mm_58.9569g_0.4n_Ender3_4h9m.gcode
or
- **6x** FlapIn_Single_PLA_PLA0.2mm_19.6835g_0.4n_Ender3_1h24m.gcode

## Other parts (nice to have)
- **1x** FlapHelpers_PLA_PLA0.2mm_24.4147g_0.4n_Ender3_2h49m.gcode