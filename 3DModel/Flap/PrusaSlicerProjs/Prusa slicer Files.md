# Prusa slicer project Files

- Nozzle diameter: 0.4mm
- Layer Height: 0.2mm
- Filament: PrintMe PLA from EazyFilament
- Nozzle temperature: 210°C
- Bed temperature: 40°C

## Bottom parts
- **Corner**
    - **1x** FlapBottomCorner_PLA.3mf 
    or 
    - **2x** FlapBottomCorner_Single_PLA.3mf
- **Down**
    - **1x** FlapBottomDown_PLA.3mf 
- **Right, Left and Up**
    - **3x** FlapBottomRightLeftUp_PLA.3mf

## Top parts
- **Corner**
    - **1x** FlapCorner_PLA.3mf
    or
    - **2x** FlapCorner_Single_PLA.3mf
- **Down**
    - **1x** FlapDown_PLA.3mf
- **Left**
    - **1x** FlapLeft_PLA.3mf
- **Right**
    - **1x** FlapRight_PLA.3mf
- **Up**
    - **1x** FlapUp_PLA.3mf

## Other parts
- **2x** FlapIn_PLA.3mf
or
- **6x** FlapIn_Single_PLA.3mf

## Other parts (nice to have)
- **1x** FlapHelpers_PLA.3mf