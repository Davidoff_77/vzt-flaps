# STL Files

## Bottom parts
- FlapBottomCorner.stl
    - 2 flaps placed in the first row on the 1st and 3rd position
- FlapBottomDown.stl
    - 1 flap placed in the second row on the 2nd position (in the middle)
- FlapBottomRightLeftUp.stl
    - 1 flap placed in the first row ont the 2nd position (in the middle) and 2 flaps placed in the second row on the 1st and 3rd position and in the 

## Top parts
- FlapCorner.stl
    - 2 flaps placed in the first row on the 1st and 3rd position
- FlapDown.stl
    - 1 flap placed in the second row on the 2nd position (in the middle)
- FlapLeft.stl
    - 1 flap placed in the second row on the 1st position
- FlapRight.stl
    - 1 flap placed in the second row on the 3st position
- FlapUp.stl
    - 1 flap placed in first row ont the 2nd position (in the middle)

## Other parts
- FlapIn.stl
    - inside part in the all flaps
- FlapTrn.stl
    - helpful part for gluing top and bottom part
- FlapStandTest.stl
    - Test stand for flap functionality testing